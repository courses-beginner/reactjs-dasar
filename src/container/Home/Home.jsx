import React, {Component} from 'react'
import YoutubeComp from '../../component/YoutubeComp/YoutubeComp'


class Home extends Component {
    render() {
        return(
            <div>
            <p>Youtube Component</p>
            <hr/>
            <YoutubeComp 
            time="10:00" 
            title="#1 Tutorial Dasar Rect JS"
            desc="Ditonton 1x Tayang 24 Hari yang lalu"/>
            <YoutubeComp 
            time="20:00" 
            title="#2 Tutorial Dasar Rect JS"
            desc="Ditonton 1x Tayang 24 Hari yang lalu"/>
            <YoutubeComp 
            time="30:00" 
            title="#3 Tutorial Dasar Rect JS"
            desc="Ditonton 1x Tayang 24 Hari yang lalu"/>
            <YoutubeComp />
            </div>
        )
    }
}

YoutubeComp.defaultProps = {
    time: "00:00",
    title: "Tidak ada judul",
    desc: "Ditonton 0 Tayangl 0 Hari yang laln"
}

export default Home;